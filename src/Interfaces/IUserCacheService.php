<?php

namespace SergeyMZR\Social\Interfaces;

interface  IUserCacheService {


    /*
     *
     * Возвращает информацию об пользователе из кэша.
     * Если в кэше нет, то получим из DB
     * Если, нет данных, то false
     *
     *
     *
     */
    public function byId($nUserId, IUserDBService $sUserDBService);
    //{
    //    if($arAuthor === false){
    //    $arAuthor = $this->_userDBService->byId($nAuthorId);
    //    $this->_userCacheService->add($arAuthor);
    //    }
    //}


    public function add($arUser);

}