<?php

namespace SergeyMZR\Social\Interfaces;

interface  IUserDBService {


    /**
     *
     * Возвращает информацию об пользователе из mysql
     *
     * @param $nUserId
     * @return mixed
     */
    public function byId($nUserId);

}