<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 31.01.2016
 * Time: 21:14
 */

namespace SergeyMZR\Social\Interfaces;


interface ICommentsChanelFeed {

    /*
     * Лента комментариев.
     *
     * Храним в tarantool
     *
     *   Структура:
     *      - id - id комментария (см. ICommentsDBService).
     *      - entity_type - id типа канала (сообщение, статья и т.п.)
     *      - entity_id - (строка) id щбъекта
     *
     *  Назначение:
     *      1. Быстрое получение id последних 3 комментариев
     *      2. Определить: если для канала, новые комментарии и сколько их.
     *      3. Получить заданное кол-во комментариев + смещение. Порядок: id, т.е. по дате создания.
     */
    public function add();

    public function delete();

    public function getLast();

    public function get();

}