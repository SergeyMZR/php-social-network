<?php

namespace SergeyMZR\Social\Interfaces;

interface IUserFeedService {


    /* ************************************
    *
    *  Лента пользователя
    *
    *   Структура:
    *      - id - автоикремент.
    *      - postId - id сообщения
    *      - userFeedId - id пользователя. Т.е. чья эта лента
    *      - updatedAt - дата последнего добавления комментария для этого сообщения. Инициализируется createdAt
    *
    *    Использование:
    *      - Лента автора - получить все сообщения в порядке updatedAt. Т.е. возможность следить за общением.
    *
    *  Что попадает в эту ленту:
    *      1. Все сообщения владельца ленты.
    *      2. Все сообщения пользователей, на которых подписан владелец ленты.
    *      3. На сообщения, на которые он подписан.
    *
    *      todo Определиться возможность подписки/отписки от конкретного сообщения
    *
    *   Обновление ленты (updatedAt)
    *      Если для сообщения (которое есть в ленте) добавлили комментарий, то обновиться updatedAt
    *      Если создатель комментария и владелец ленты одно лицо, то updatedAt не изменяется.
    *
    */


    /*
     * Добавить сообщение в ленты пользователей.
     *  1. Добавим сообщение в ленту автора.
     *  2. Добавим сообщение в ленты пользователей, которые подписаны на $nAuthorId
     */
    public function add($nAuthorId, $nPostId, $dtPostCreatedAt);


    /*
    * --Подымем сообщение в лентах пользователей, в которых есть данное сообщение. Если автор комментария и ленты одно лицо, то не подымаем
    */
    public function update($nAuthorId, $nPostId, $dtDateUpdate);

//    //общая лента
//    public function addToCommonFeed($nAuthorId, $nPostId, $nPostTypeId, $nPurposeId, $dtCreatedAt);
//    //
//
//    //лента пользователя
//    public function addToAuthorFeed($nAuthorId, $postId, $dtUpdatedAt);
//    public function addToSubscribersFeed($nAuthorId, $postId, $dtUpdatedAt);
//    public function updateInAuthorFeed($nAuthorId, $nPostId, $dtUpdatedAt);
//
//
//    //лента комментариев
//    public function addToCommentsFeed($nCommentId, $arUsersId, $dtCreatedAt);

}