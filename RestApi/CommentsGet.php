<?php

namespace SergeyMZR\RestApi;
/*
 * Для заданного канала получить комментарии: кол-во + смещение
 * Порядок: сначала старые, потом новые
 */
use SergeyMZR\Social\Apps\CommentsApp;

class CommentsGet {
    public function action(){


        /*
         *  Проверяем все обязательные поля в $_POST и приводим их в соотвествии с заданным типом
         */
        $arResult = Utils::validate(array(
            //канал, по которому получим комментарии
            "chanel"=>array("type"=>"json"),

            //сколько нужно получить
            "count"=>array("type"=>"int"),
            //смещение
            "offset"=>array("type"=>"int"),
        ), $_POST);

        if($arResult === false){
            echo "error";
            return false;
        }

        (new CommentsApp())->getLast();

        /*
         * Ответ в JSON
         */

    }
}