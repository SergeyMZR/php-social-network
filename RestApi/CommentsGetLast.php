<?php


namespace SergeyMZR\RestApi;

/*
 * Для переданных каналов получить последние 3 комментария
 * Ответ в JSON
 */
use SergeyMZR\Social\Apps\CommentsApp;

class CommentsGetLast {
    public function action(){


        //1. Проверяем все обязательные поля в $_POST и приводим их в соотвествии с заданным типом
        $arResult = Utils::validate(array(
            //массив каналов, по которым нужно получить последние 3 комментария
            "chanels"=>array("type"=>"json"),
        ), $_POST);



        (new CommentsApp())->getLast($arChanels);
        //возвращаем в формате json

    }
}