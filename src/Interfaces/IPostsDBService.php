<?php

namespace SergeyMZR\Social\Interfaces;


interface IPostsDBService {
    public function save($nAuthorId, $sAuthorName, $postTypeId, $sTitle, $sMessage, $arPhotos, $arVideos, $arAttaches, $arSections, $arTags, $entityId, $entityType, $dtPostCreatedAt);

    public function update();

    public function markAsDeleted($nPostId, $nAuthorId, $isAdmin);

    public function delete($nCommentId, $nAuthorId, $isAdmin);

    public function incrementComments();
}