<?php


namespace SergeyMZR\RestApi;

/*
 *
 * Добавить комментарий
 * Ответ в JSON
 */
use SergeyMZR\Social\Apps\CommentsApp;

class CommentsAdd {


    public function action(){


       /*
        * -- Проверяем все обязательные поля в $_POST и приводим их в соотвествии с заданным типом
        */

        $arResult = Utils::validate(array(

            /*
            *   $arChanel канал, для которого пишеться комментарий
                  $arChanel = array(
                    "entity_type"=> 1,
                    "entity_id"=> "1256",
                    "url"=> "/people/user/1/blog/120516/",
                    "title"=>"Дневник питания за 30-01-2016"
                );
            */

            "chanel"=>array("type"=>"json"),
            "message"=>array("type"=>"string", "maxLength"=>1000),//текст сообщения
            "mentions"=>array("type"=>"json", "НеОбязательный"=>true),//упоминание пользователей
            "photos"=>array("type"=>"json", "НеОбязательный"=>true),//фотографии
            "videos"=>array("type"=>"json", "НеОбязательный"=>true),//видео
            "attaches"=>array("type"=>"json", "НеОбязательный"=>true),//прикрепленная ифнормация
            "mentions"=>array("type"=>"json", "НеОбязательный"=>true),//прикрепленная ифнормация
        ), $_POST);

        if($arResult === false){
            echo "error";
            return false;
        }


        /*
         * -- todo проверяем авторизацию
         */


        /*
         * -- todo Провереям на спам
         * На массовое выполнение запросов
         * Проверка содержания текста
         */


        /*
         * -- Добавим комментарий
         */

        $lResult = CommentsApp::add(/*Передаем все параметры*/);


        if($lResult === false){
            //Ответ в JSON
            echo "error:" . $lResult;
            return false;

        }else{
            //Ответ в JSON
            echo "Ответ - успешно добавлен комментарий + id";
            return false;

        }



    }
}