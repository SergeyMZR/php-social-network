<?php
/*
 * Ответ в JSON
 */
class UploadPhoto {

    //загрузка фото будет осуществляться http://getuikit.com/docs/upload.html
    public function action(){


        //1. Проверяем все обязательные поля в $_POST и приводим их в соотвествии с заданным типом
        $arResult = Utils::validate(array(

        ), $_POST);

        if($arResult === false){
            echo "error";
            return false;
        }

        //2. Проверяем подпись
        if(Utils::validateSSO($_POST) === false){
            echo "error";
            return false;
        }

        // Получим пользователя, если его нет, то будет создан
        $user = UserService::bySSO($arResult["sso"]);

        // Получим канал, если его нет, то будет создан
        $chanel = Chanel::byChanel($arResult["chanel"]);


        /*
         *  Сохраняем на диск. Предворительно уменьшив качество, уменьшив разрешение - маскимум ширина 600px
         *  Пример, можно здесь взять https://github.com/blueimp/jQuery-File-Upload/tree/master/server/php
         */


        /*
         * Наименование файла  = uid()
         */

        /**
         *  Создаем превью 100*100
         */

        /**
         *  Отправляем на selectel https://selectel.ru/services/cloud-storage/
         *
         */
        $path = $user->id . "/" . $chanel->id . "/" ;

        /**
         * Сохраняем в БД.
         * Там храним.
         * id
         * userId
         * chanelId
         * name - uid()
         * размер фото в кб.
         * idCloud - Пока будет равно 1. Т.е. соотвествует selectel. Но если вдруг придется перейти на другое облако, я смогу поменять id
         */

        //ответ
        echo array("id"=>1, "userId"=>'','chanelId'=>'','name'=>'' , "url"=>"ссылка");


    }
}