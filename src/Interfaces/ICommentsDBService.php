<?php
namespace SergeyMZR\Social\Interfaces;

interface  ICommentsCacheService {

    /*
     * Храние комментариев в БД
     */

    public function save($nAuthorId, $nEntityType,$sEntityId, $sMessage, $arPhotos, $arVideos, $arAttaches);

    public function delete($nCommentId, $nAuthorId, $isAdmin);

    public function get();

}