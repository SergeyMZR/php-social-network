<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 27.01.2016
 * Time: 16:29
 */

class DeletePhoto {

    //удаляем фотографию
    public function action(){


        //1. Проверяем все обязательные поля в $_POST и приводим их в соотвествии с заданным типом
        $arResult = Utils::validate(array(

        ), $_POST);

        if($arResult === false){
            echo "error";
            return false;
        }

        //2. Проверяем подпись
        if(Utils::validateSSO($_POST) === false){
            echo "error";
            return false;
        }

        // Получим пользователя, если его нет, то будет создан
        $user = UserService::bySSO($arResult["sso"]);

        // Получим канал, если его нет, то будет создан
        $chanel = Chanel::byChanel($arResult["chanel"]);


        /*
         * Ищем в БД.
         *
         * Если такого файла нет, то error
         * Если есть, то удаляем его с облака и из базы данных
         */

        //ответ
        echo array("ok");


    }
}