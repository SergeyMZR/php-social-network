<?php
namespace SergeyMZR\Social\Interfaces;

interface  ICommentsDBService {


    /*
     * Храние комментариев в кэше
     *
     * Назначение: быстрое получение всей информации об комментарии, без запросов в БД
     */

    public function save($nAuthorId, $nEntityType,$sEntityId, $sMessage, $arPhotos, $arVideos, $arAttaches);

    public function delete($nCommentId, $nAuthorId, $isAdmin);

}