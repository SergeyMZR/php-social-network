<?php

namespace SergeyMZR\Social\Interfaces;


interface IPostsCacheService {

    /*
     * todo textParser
     */
    public function save($nPostId, $nAuthorId, $sAuthorName, $postTypeId, $sTitle, $sMessage, $arPhotos, $arVideos, $arAttaches, $arSections, $arTags, $entityId, $entityType, $dtPostCreatedAt);

    public function update();

    public function markAsDeleted();

    public function incrementComments();
}