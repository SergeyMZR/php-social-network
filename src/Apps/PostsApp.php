<?php

namespace SergeyMZR\Social\Apps;

use SergeyMZR\Social\Interfaces\ICommonFeedService;
use SergeyMZR\Social\Interfaces\IMentionsService;
use SergeyMZR\Social\Interfaces\IPostsCacheService;
use SergeyMZR\Social\Interfaces\IPostsDBService;
use SergeyMZR\Social\Interfaces\IUserCacheService;
use SergeyMZR\Social\Interfaces\IUserFeedService;
use SergeyMZR\Social\Interfaces\IUserDBService;
use SergeyMZR\Social\TextParser\TextParser;

class PostsApp {


    /**
     * @var $_textParser TextParser
     */
    protected $_textParser;

    /**
     * @var $_userCacheService  IUserCacheService
     */
    protected $_userCacheService;

    /**
     * @var $_userDBService  IUserDBService
     */
    protected $_userDBService;


    /**
     * @var $_postsDBService  IPostsCacheService
     */
    protected $_postsCacheService;

    /**
     * @var $_postsDBService  IPostsDBService
     */
    protected $_postsDBService;

    /**
     * @var $_mentionsService  IMentionsService
     */
    protected $_mentionsService;

    /**
     * @var $_commonFeedService  ICommonFeedService
     */
    protected $_commonFeedService;

    /**
     * @var $_userFeedService  IUserFeedService
     */
    protected $_userFeedService;




    public function addPost($nAuthorId, //id пользователя

                        //id типа сообщения
                        $postTypeId,
                        $sTitle,//Заголовок сообщения (может быть не задан)
                        $sMessage, //текст сообщения, может быть пустой, но в этом случае один из параметров ($arPhotos или $arVideos или $arAttaches) не должны быть пустыми
                        //список фоток, прикрепленных к сообщению, сохраняется в бд ввиде json. (Не обязательно)
                        $arPhotos = array(
                            array("url"=>"", "id"=>1),
                            array()
                        ),
                        //список видео, прикрепленных к сообщению, сохраняется в бд ввиде json. (Не обязательно)
                        $arVideos = array("url1", "url2"),
                        //дополнительные данные json, сохраняется в бд ввиде json. (Не обязательно)
                        $arAttaches = array(),
                        //упоменения, в бд не сохраняется  (Не обязательно)
                        $arMentions = array(),
                        //id разделов, в котроые входит сообщение. (Не обязательно)
                        $arSections = array(12,34),
                        //теги сообщения (Не обязательно)
                        $arTags = array("мое питания", "спорт"),
                        //Дополнительные данные. Связь с сервером приложения "Дневники". (Не обязательно)
                        $entityId = 0,
                        $entityType = 0
    ){


       /*
       * -- Подготовим текст для записи в БД
       */
        $sMessage = $this->_textParser->prepareForSaveInDB($sMessage);
        $sTitle = $this->_textParser->toSimpleText($sTitle);

        /*
         *  -- Проверим, что сообщение не пустое
         *  Что-то должно быть не пустым: $sMessage или $arPhotos или $arVideos или $arAttaches
         */


        /*
         * -- Получим данные об авторе сообщения
         */
        $arAuthor = $this->_userCacheService->byId($nAuthorId, $this->_userDBService);
        if($arAuthor === false){
            return false;//ошибка, не найден пользователь
        }

        $sAuthorName = $arAuthor['name'];
        $nPurposeId = $arAuthor['purposeId'];


        /*
         * -- Сохраним сообщение в БД и в кэше
         */
        $dtPostCreatedAt = new \DateTime();
        $nPostId = $this->_postsDBService->save($nAuthorId, $postTypeId, $sTitle, $sMessage, $arPhotos, $arVideos, $arAttaches, $arSections, $arTags, $entityId, $entityType, $dtPostCreatedAt);
        $this->_postsCacheService->save($nPostId, $nAuthorId, $sAuthorName, $postTypeId, $sTitle, $sMessage, $arPhotos, $arVideos, $arAttaches, $arSections, $arTags, $entityId, $entityType, $dtPostCreatedAt);

        /*
         * --Добавим сообщение в общую ленту
         */
        $this->_commonFeedService->add($nAuthorId, $nPostId, $postTypeId, $nPurposeId, $dtPostCreatedAt);


        /*
         * --Добавим сообщение в ленту пользователям: автору и пользователям, которые подписаны на данного автора
         */
        // todo $this->_userFeedService->add($nAuthorId, $nPostId, $dtPostCreatedAt);

        /*
        * -- Добавим упоменания для всех, кто указан в $arMentions
        */
        //todo $this->_mentionsService->addMentionsFromBlog($arMentions, $nAuthorId, $sAuthorName, $sTitle);

        /*
        * todo -- Отправим сообщение на индексацию в поиск
        */

        /*
        * -- todo В очередь добавляем задачу, по созданию карты сайта
        */


    }


    public function updatePost(){
        /*
         * -- сохраним
         * Внутри каждой функции проверим: либо это должен быть владелей или $isAdmin=true
         */

        $this->_postsDBService->update($nPostId, $nAuthorId, $isAdmin);
        $this->_postsCacheService->update($nPostId, $nAuthorId, $isAdmin);
    }


    public function deletePost(){
        /*
         * Внутри каждой функции проверим: либо это должен быть владелей или $isAdmin=true
         */
        $this->_postsDBService->markAsDeleted($nPostId, $nAuthorId, $isAdmin);
        $this->_postsCacheService->markAsDeleted();
    }



    public function hasPostNewComments(){

        /*
         * todo Проверяет наличие комментариев в сообщении
         */

    }


//
//    public function getListOfPost(){
//
//        //1. Пробуем получить из кэша
//        //2. Что не получилось получаем из БД
//
//
//    }
}