<?php


namespace SergeyMZR\RestApi;

/*
 * Удалить комментарий
 * Ответ в JSON
 */
use SergeyMZR\Social\Apps\CommentsApp;

class CommentsDelete {
    public function action(){


        /*
         * --  Проверяем все обязательные поля в $_POST и приводим их в соотвествии с заданным типом
         */
        $arResult = Utils::validate(array(
            "commentId"=>array("type"=>"int"),
            "userId"=>array("type"=>"int"),
        ), $_POST);

        if($arResult === false){
            echo "error";
            return false;
        }

        /*
        * -- todo проверяем авторизацию
        */


        /*
         * todo Имеет ли данный пользователь права администратора
         */
        $isAdmin = false;


        CommentsApp::delete($arResult["commentId"], $arResult["userId"], $isAdmin);



        /*
         * -- Ответ в формате json
         */



    }
}