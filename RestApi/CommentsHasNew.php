<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 01.02.2016
 * Time: 10:18
 */

namespace SergeyMZR\RestApi;

/*
 * Для переданных каналов получить: есть ли и сколько новых комментариев
 * Ответ в JSON
 */
use SergeyMZR\Social\Apps\CommentsApp;

class CommentsHasNew {
    public function action(){


        //1. Проверяем все обязательные поля в $_POST и приводим их в соотвествии с заданным типом
        $arResult = Utils::validate(array(
            //массив каналов, по которым нужно получить
            "chanels"=>array("type"=>"json"),
        ), $_POST);

        (new CommentsApp())->hasNew();
        //возвращаем в формате json

    }
}