<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 29.01.2016
 * Time: 16:26
 */

class LiveFeedApp {

    /*
     * Получить последние обновления из общей ленты
     * todo получить кол-во
     */
    public function getUpdateOfCommonFeed(
        $sOrderBy, // "byCreated" - по дате создания, "byUpdated" - по дате изменения
        $arFilter = array(//фильтр
            //Для общей ленты доступно, но не обязательно
            "postTypeId"=>1, //id типа сообщения
            "authorId"=> 12,
            "purposeId"=>1,
        ),
        $nCountPosts = 10, //кол-во сообщений, которые вернуть
        $dateOfLastUpdate = null//дата на которую сформирована лента в браузере человека
    ){


        //Алгоритм

        /*
         * -- Получаем из ленты последние $nCountPosts записей.
         */

        if($sOrderBy === "byCreated" || $dateOfLastUpdate === null){
            /*
             * -- Для каждого сообщения получим последние 3 записи.
             */
        }else{

            /*
             * -- Для каждого сообщения получим все комментарии, которые созданы позже $dateOfLastUpdate, но не более 10
             */
        }



    }

    public function getCountOfUpdateOfCommonFeed(
        $sOrderBy, // "byCreated" - по дате создания, "byUpdated" - по дате изменения
        $arFilter = array(//фильтр
            //Для общей ленты доступно, но не обязательно
            "postTypeId"=>1, //id типа сообщения
            "authorId"=> 12,
            "purposeId"=>1,
        )
    ){
        //аналогично getUpdateOfCommonFeed, только нужно вернуть количество новых сообщений. Комментарии не трогаем.
    }

    public function getPostCommonFeed(
        $sOrderBy, // "byCreated" - по дате создания, "byUpdated" - по дате изменения
        $arFilter = array(//фильтр
            //Для общей ленты доступно, но не обязательно
            "postTypeId"=>1, //id типа сообщения
            "authorId"=> 12,
            "purposeId"=>1,
        ),
        $nOffset = 10,//смещение
        $nCountPosts = 10, //кол-во сообщений, которые вернуть
        $dateOfLastUpdate = null//дата на котрую сформирована лента в браузере человека
    ){

    }


    /*
     * Вернуть кол-во сообщений в общей ленте.
     * Будет использоваться для построения пагинации.
     */
    public function getCountCommonFeed(
        $arFilter = array(//фильтр
            //Для общей ленты доступно, но не обязательно
            "postTypeId"=>1, //id типа сообщения
            "authorId"=> 12,
            "purposeId"=>1,
        )
    ){
        /*
         *
         */
        return 10;
    }




    public function getUpdateOfFeed2(
        $nFeedId, //1 - общая лента, 2 лента пользователя
        $sOrderBy, // "byCreated" - по дате создания, "byUpdated" - по дате изменения
        $arFilter = array(//фильтр
            //Для общей ленты доступно, но не обязательно
            "postTypeId"=>1, //id типа сообщения
            "authorId"=> 12,
            "purposeId"=>1,

            //для ленты пользователя. Обязательный фильтр
            "userFeedId"=> 12,
        )


    ){


    }




}