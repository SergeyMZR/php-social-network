<?php

namespace SergeyMZR\Social\Apps;

use SergeyMZR\Social\Interfaces\IChanelDBService;
use SergeyMZR\Social\Interfaces\ICommentsCacheService;
use SergeyMZR\Social\Interfaces\ICommentsChanelFeed;
use SergeyMZR\Social\Interfaces\ICommentsDBService;
use SergeyMZR\Social\Interfaces\ICommonFeedService;
use SergeyMZR\Social\Interfaces\IMentionsService;
use SergeyMZR\Social\Interfaces\IPostsCacheService;
use SergeyMZR\Social\Interfaces\IPostsDBService;
use SergeyMZR\Social\Interfaces\IUserCacheService;
use SergeyMZR\Social\Interfaces\IUserCommentsFeedService;
use SergeyMZR\Social\Interfaces\IUserFeedService;
use SergeyMZR\Social\Interfaces\IUserDBService;
use SergeyMZR\Social\TextParser\TextParser;

class CommentsApp {


    /**
     * @var $_textParser TextParser
     */
    protected $_textParser;

    /**
     * @var $_userCacheService  IUserCacheService
     */
    protected $_userCacheService;

    /**
     * @var $_userDBService  IUserDBService
     */
    protected $_userDBService;


    /**
     * @var $_postsDBService  IPostsCacheService
     */
    protected $_postsCacheService;

    /**
     * @var $_postsDBService  IPostsDBService
     */
    protected $_postsDBService;

    /**
     * @var $_mentionsService  IMentionsService
     */
    protected $_mentionsService;

    /**
     * @var $_userFeedService  IUserFeedService
     */
    protected $_userFeedService;


    /**
     * @var $_commentsDBService  ICommentsDBService
     */
    protected $_commentsDBService;


    /**
     * @var $_chanelService  IChanelDBService
     */
    protected $_chanelService;


    /**
     * @var $_commentsChanelFeed  ICommentsChanelFeed
     */
    protected $_commentsChanelFeed;


    /**
     * @var $_userCommentsFeedService  IUserCommentsFeedService
     */
    protected $_userCommentsFeedService;


    /**
     * @var $_commentsCacheService  ICommentsCacheService
     */
    protected $_commentsCacheService;






    public function add(
        //автор комментария
        $nAuthorId =1,
        //описание канала: entity_type - id типа канала (сообщение, статья и т.п.)   entity_id - id объекта (Ввиде строки!)
        $arChanel= array('entity_type'=>1, 'entity_id'=>"23", 'url'=>'', 'title'=>''),
        //само сообщение
        $sMessage,
        //id комментария, на который ответили
        $nParentCommentId = null,
        $arPhotos = array(),
        $arVideos = array(),
        $arAttaches = array(),
        $arMentions = array()//кому ответили + упоменания
    ){

        /*
        * -- Подготовим текст для записи в БД
       */
        $sMessage = $this->_textParser->prepareForSaveInDB($sMessage);

        /*
        *  -- Проверим, что сообщение не пустое
        *  Что-то должно быть не пустым: $sMessage или $arPhotos или $arVideos или $arAttaches
        */

        /*
        * -- Получим данные об авторе сообщения
        */
        $arAuthor = $this->_userCacheService->byId($nAuthorId, $this->_userDBService);

        if($arAuthor === false){
            return false;//ошибка, не найден пользователь
        }

        $sAuthorName = $arAuthor['name'];
        $nPurposeId = $arAuthor['purposeId'];

        /*
        * todo -- Получим канал, если его нет, то будет создан
        */
//        if($this->_chanelService->byChanel($arChanel) === false) {
//            $this->_chanelService->create($arChanel);
//        }


        /*
         * -- Сохраним комментарий
         */
        if($this->_commentsDBService->save($nAuthorId, $arChanel['entity_type'],$arChanel['entity_id'], $nParentCommentId, $sMessage, $arPhotos, $arVideos, $arAttaches) === false){
            return "Описание ошибки";
        }else{

            /*
             * Добавим комментарий в ленту комментариев
             */
            $this->_commentsChanelFeed->add();

            /*
             * -- Сохраним комментарий в кэше
             */
            $this->_commentsCacheService->save();

            /*
             * -- Добавим комментарий в общую ленту комментариев
             */
            //todo $this->_commentsChanelFeed->add($nCommentId, $arChanel['entity_type'],$arChanel['entity_id']);


            if($arChanel['entity_type'] === 'xxx код сообщения блога'){

                $nPostId = $chanel->entity_id;
                /*
                 * --Подымем сообщение в лентах пользователей, в которых есть данное сообщение. Если автор комментария и ленты одно лицо, то не подымаем
                 */
                //todo $this->_userFeedService->update($nAuthorId, $nPostId , $dtDateUpdate);


                //todo $this->_userCommentsFeedService->add($nCommentId, $arUsersId, $dtCreatedAt);

                /*
                * -- Увеличим кол-во комметариев в сообщениях
                */
                //todo $this->_postsCacheService->incrementComments($nPostId);

                //todo $this->_postsDBService->incrementComments($nPostId);

            }



            /*
             * -- todo Если $arMentions не пустой, то в очередь добавляем задачу упомянуть этих людей.
             */


            return true;
        }

    }


    /*
     * Удалить комментарий
     */
    public function delete($nCommentId, $nAuthorId, $isAdmin){

        /*
         *-- удаляем комментарий
         */
        if($this->_commentsDBService->delete($nCommentId, $nAuthorId, $isAdmin) === false){
            return "Описание ошибки";
        }else{


            $this->_commentsCacheService->delete($nCommentId);


            /*
             * -- todo Если в комментарии были фотки, то в очередь добавляем задачу удалить эти фотки.
             */


            /*
             * -- todo удаляем из ленты комментариев
             */

            return true;
        }
    }


    /*
     * Послучить последние 3 комментария, для массива каналов
     */
    public function getLast($arChanels){

        /*
         * -- Получим id, для каждого канала
         */
        $arResult = $this->_commentsChanelFeed->getLast();

        /*
         * -- Попробуем получить комментарии из кэша
         */

        $this->_commentsCacheService->get($arResult);


        /*
        * -- Которых нет в кэше, получим из БД
        */

        $this->_commentsDBService->get($arResult);

    }



    /*
     * Есть ли новые комментарии для заданных каналов и сколько их
     */
    public function hasNew($arChanels){

    }


    /*
     * Полдучить заданное кол-во комментариев с учетом смещения
     */
    public function get($arChanel, $nCount, $nOffset){


        /*
       * -- Получим id, для каждого канала
       */
        $arResult = $this->_commentsChanelFeed->get();

        /*
         * -- Попробуем получить комментарии из кэша
         */

        $this->_commentsCacheService->get($arResult);


        /*
        * -- Которых нет в кэше, получим из БД
        */

        $this->_commentsDBService->get($arResult);

    }





}

